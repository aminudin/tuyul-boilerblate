const chalk = require('chalk');
const figlet = require('figlet');
const fs = require('fs');

const readlineSync = require('readline-sync');



const CURR_DIR = process.cwd();

console.log(
    chalk.yellow(
        figlet.textSync('Tuyul Boilerplate', { horizontalLayout: 'full' })
    )
);


console.log("")
console.log("")
const projectName = readlineSync.question('Masukan nama project bot mu : ');
const step = readlineSync.question('Berapa kali step dalam bot mu ?, ex Login - Logout itu 2 : ');
const funC = []
for (let index = 0; index < step; index++) {
    const endpoint = readlineSync.question(`Masukan nama function ke ${index + 1} : `);
    funC.push(endpoint)
}
contenTip = ["application/json", "application/x-www-form-urlencoded"]
const contentType = readlineSync.keyInSelect(contenTip, 'Pilih content type');
method = ["get", "post"]
const projectChoice = readlineSync.keyInSelect(method, 'Pilih content type');
console.log("")
console.log("")



function createDirectoryContents(templatePath, newProjectPath, method, contentType) {
    const filesToCreate = fs.readdirSync(templatePath);

    filesToCreate.forEach(file => {
        const origFilePath = `${templatePath}/${file}`;

        // get stats about the current file
        const stats = fs.statSync(origFilePath);

        if (stats.isFile()) {
            const contents = `
            const fetch = require("node-fetch");

            ${funC.map(funfun =>
                `const ${funfun} = () =>
                new Promise((resolve, reject) => {
                    const bod = {}
                    fetch(
                        "masukan url disini",
                        {
                            headers: {
                                "content-type": "${contentType}",
                            },
                            body: ${contentType === "application/json" ? `JSON.stringify(bod)` : ``},
                            method: "${method}",
                        }
                    )
                        .then(res => res.text())
                        .then(re => resolve(re));
                });`+ "\n"
            )}
            
            (async () => {
            
                try {
                    ${funC.map(funfun =>
                `const var${funfun} = await ${funfun}();`
            )}
                    
                        console.log()
                } catch (e) {
                    console.log(e);
                }
            })();
            `

            const writePath = `${CURR_DIR}/${newProjectPath}/index.js`;
            fs.writeFileSync(writePath, contents, 'utf8');
            console.log("Project bot berhasil dibuat di", writePath)
        } else if (stats.isDirectory()) {
            fs.mkdirSync(`${CURR_DIR}/${newProjectPath}/${file}`);

            // recursive call
            createDirectoryContents(`${templatePath}/${file}`, `${newProjectPath}/${file}`);
        }
    });
};

(async () => {

    const templatePath = `${__dirname}/lib/templates/json/${method[projectChoice]}`;

    fs.mkdirSync(`${CURR_DIR}/${projectName}`);

    createDirectoryContents(templatePath, projectName, method[projectChoice], contenTip[contentType]);
})();