const fetch = require("node-fetch");

const firstScrape = () =>
    new Promise((resolve, reject) => {
        const bod = {}
        fetch(
            "https://accounts.google.com/_/signup/accountdetails?hl=en&_reqid=28744&rt=j",
            {
                headers: {
                    "content-type": "application/json",
                },
                body: JSON.stringify(bod),
                method: "POST",
            }
        )
            .then(res => res.text())
            .then(re => resolve(re));
    });

(async () => {

    try {
        const firstFunc = await firstScrape();

        console.log(firstFunc);
    } catch (e) {
        console.log(e);
    }
})();
