const inquirer = require('inquirer');
const fs = require('fs');

const CHOICES = fs.readdirSync(`${__dirname}/templates`);

module.exports = {

    tuyulData: () => {
        const questions = [
            {
                name: 'project-name',
                type: 'input',
                message: 'Masukan nama project tuyul kalian :',
                validate: function (input) {
                    if (/^([A-Za-z\-\_\d])+$/.test(input)) return true;
                    else return 'Project name may only include letters, numbers, underscores and hashes.';
                }
            },
            {
                name: 'endpoint',
                type: 'input',
                message: 'masukan url endpoint target :',
            },
            {
                name: 'content-type',
                type: 'list',
                message: 'masukan content type :',
                choices: ["application/json", "application/x-www-form-urlencoded"]
            },
            {
                name: 'method',
                type: 'list',
                message: 'Pilih method yang akan digunakan :',
                choices: ["get", "post"]
            }
        ];
        return inquirer.prompt(questions);
    },
}